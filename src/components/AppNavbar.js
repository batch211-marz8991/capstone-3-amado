import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import logo from "../assets/logo.png"

export default function AppNavbar(){


  const { user } = useContext(UserContext);
return(
    <Navbar bg="black" expand="lg" variant="dark">
        <Container classname="h-20">
        <div className="h-12 flex items-center">
          <img src={logo} classname="object-cover" alt="logo"/>
        </div>
          <Navbar.Toggle bg="light" classname="border-0" aria-controls="basic-navbar-nav" style={{color: 'green'}}/>
          <Navbar.Collapse id="basic-navbar-nav" >
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/" className="text-white">Home</Nav.Link>
              <Nav.Link as={Link} to="/products" className="text-white">Products</Nav.Link>
             
              {(user.id !== null) ?
                <Nav.Link as={Link} to="/logout" className="text-white">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={Link} to="/login" className="text-white">Login</Nav.Link>
                  <Nav.Link as={Link} to="/register" className="text-white">Register</Nav.Link>
                </>
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
  )
}