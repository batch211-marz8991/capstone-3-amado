import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
  return(
    <Row className = "mt-3 mb-3">
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
          <Card.Title>Save Up To 50%</Card.Title>
          <Card.Text>
          Come quik and avail the 50% discount on selected products.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
          <Card.Title>Hot Products</Card.Title>
          <Card.Text>
          Grab our trending available products.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
          <Card.Title>Be Part of Our Community</Card.Title>
          <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    </Row>
    );
}
