import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	const data = {
		title:"Level up your PC at On-Time-Tech!",
		content: "We're here to help you find the gadgets you need for your lifestyle.",
		destination: "/products",
		label: "Shop Now!"
	}

	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>
	)
}